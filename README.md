# APP安装
## 手机安装 

1. **投屏助手.apk（原ScreenCastTool）** （用于强制横屏，安装完后先打开,开启**在其他应用上层显示**的权限和**访问通知**权限，并设置成**允许后台运行**，否则投屏过程中可能会被系统杀掉导致某些app不能横屏显示或在副屏上显示异常）
2. **Nova桌面.apk（或其他第三方桌面）** （作为投屏桌面。有些小米手机装nova桌面后会导致全面屏手势失效，也可以安装其他桌面。不用设置为默认桌面，只要在投屏后绑定到导航栏即可）
## 车机安装
1. **手机投屏.apk**

## 下载地址

[投屏助手.apk](https://gitee.com/twtvfhpfm/android-cast-a/releases/download/v5.2.3/%E6%8A%95%E5%B1%8F%E5%8A%A9%E6%89%8B-v1.4.apk)

[Nova桌面.apk](https://gitee.com/twtvfhpfm/android-cast-a/releases/download/v5.0.4/Nova.apk)

[手机投屏](https://gitee.com/twtvfhpfm/android-cast-a/releases)


# 准备工作
1. 手机开启USB调试，并打开**仅充电时允许USB调试**；如果是小米手机需要开启**USB调试（安全设置）**，oppo手机需要关闭**权限监控**，否则车机无法反控手机
2. 手机上打开ScreenCastTool，开启**在其他应用上层显示**的权限和**访问通知**的权限，并设置允许**后台运行和自启动**

# 有线投屏
用数据线连接车机和手机，车机上会弹出“允许手机投屏访问该usb设备”的弹窗，点击允许，**如果没有这一步弹窗，可能是车机做了限制**

点击有线投屏后，**初次连接手机上会弹出允许调试的弹窗，设成永久允许，**
此时应该能在车机上看到手机屏幕，并能通过车机控制手机

如果出现投屏失败可以插拔USB后重试

# 无线投屏
首先将手机和车机连到一个局域网，最好使用5G频段的wifi，
![wireless2](img/wireless2.png)

然后在上图横线上填写手机ip地址，点击“无线投屏”

![wireless1](img/wireless1.png)

第一次连接需要开启无线调试，这一步需要连接数据线，连好后点击“打开无线调试”，或者通过开发人员选项里的无线调试进行配对。
![pairinput](img/inputpair.png)
![pairwifi](img/pairwifi.jpg)

如果看到开启成功的提示，再点击“无线投屏”即可，第一次手机上会弹窗要点击允许调试

若手机ip地址填写错误，也会弹出无线调试未开启的弹窗

成功连接一次后，后面只需要点击“无线投屏”就可以一键投屏

# 使用技巧
- 三指飞屏：非镜像投屏下，三指划动车机屏幕，可以将手机上当前运行的应用流转到车机上
- 导航栏设置：先打开某个应用，再长按导航栏上的快捷键3秒左右，就可以重新绑定快捷应用，支持设置的有home, 地图， 音乐，自定义app这4个按钮；如果绑定失败就用镜像投屏的方式再试一下
- 无感连接：只支持无线投屏，ScreenCastTool打开后会发送广播信号，车机端接收到信号后会开始无线投屏，可以在手机上设置智能场景，比如连上车机蓝牙或wifi后自动打开ScreenCastTool
- 分屏：投屏设置中开启分屏及调整比例，然后双击小房子就可以实现分屏，目前只在扩展投屏下支持分屏
- 扩展投屏：在设置中关闭镜像投屏后就变成扩展投屏，手机和车机可以独立操作，手机上会弹出一个辅助屏的小窗口； 如果不想要这个小窗口，可以在投屏助手中创建虚拟屏，但是虚拟屏打开应用的时候有时候会在手机上打开，需要用三指飞屏进行流转，或者杀掉app后再打开，导航栏上的应用不受影响。虚拟屏整体体验不如辅助屏，唯一的好处是没有小窗口



# 常见问题
- 投屏结束后手机上小窗口未关闭：
  正常结束投屏时会自动关闭小窗口，如果未关闭，可以在手机开发人员选项“模拟辅助显示设备”关闭
- 扩展投屏只显示半屏：
  ScreenCastTool没生效，需要赋予“在其他应用上层显示”和“后台运行”的权限
  ![harf_screen](img/half_screen.png)

